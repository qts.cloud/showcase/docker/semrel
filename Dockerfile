FROM node:lts-alpine

RUN npm install -g semantic-release@17 @semantic-release/changelog @semantic-release/commit-analyzer @semantic-release/release-notes-generator @semantic-release/git @semantic-release/gitlab @semantic-release/exec

ENTRYPOINT [ "/usr/bin/env" ]